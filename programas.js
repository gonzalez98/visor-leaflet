class Mapa{

     constructor(){
        this.simon = [4.659120, -74.093322];
        this.milenio = [4.597630, -74.081460];
        this.salitre = [4.666189996990966, -74.08874988555908];
        this.panaderia = [4.623926228403667, -74.20838713645934];
        this.mymap;
        this.PanIcono = L.icon({
            iconUrl: 'pan.png',
            iconSize:     [60, 90], // size of the icon
            iconAnchor:   [20, 90], // point of the icon which will correspond to marker's location
            popupAnchor:  [-3, -76] // point from which the popup should open relative to the iconAnchor
        });
        this.ParqueIcono = L.icon({
            iconUrl: 'parque.png',
            iconSize:     [50, 90], 
            iconAnchor:   [20, 90], 
            popupAnchor:  [-3, -76]  
        });
     }
    initMap(){
        this.mymap = L.map('visor').setView([4.6756641755743855, -74.09591674804688], 11);
        L.tileLayer('https://api.mapbox.com/styles/v1/{id}/tiles/{z}/{x}/{y}?access_token={accessToken}', {
        attribution: 'Map data &copy; <a href="https://www.openstreetmap.org/">OpenStreetMap</a> contributors, <a href="https://creativecommons.org/licenses/by-sa/2.0/">CC-BY-SA</a>, Imagery © <a href="https://www.mapbox.com/">Mapbox</a>',
        maxZoom: 18,
        id: 'mapbox/streets-v11',
        tileSize: 512,
        zoomOffset: -1,
        accessToken: 'pk.eyJ1IjoiZ29uemFsZXo5OCIsImEiOiJja2Nxcm9rZzkxNmMwMnpvMnE2ajEzemtlIn0.Gtxw7Iv8CyD_jqqZATFjsA'
        }).addTo(this.mymap);
    }
    marcador(){
        let miElemento = event.target;
        if (miElemento.value === "Parque Simon Bolivar") {
            var marker = L.marker(this.simon, {icon: this.ParqueIcono}).addTo(this.mymap);
            marker.bindPopup("Parque Simon Bolivar").openPopup();
            var polygon = L.polygon([
                [4.657196894282447, -74.10027533769608],
                [4.652475736651508, -74.09355103969573],
                [4.653074571536296, -74.09207582473755],
                [4.653887275208007, -74.091437458992],
                [4.65898269196767, -74.08920049667357],
                [4.660052029727772, -74.08926486968994],
                [4.6650030423484, -74.092698097229],
                [4.664179658219405, -74.0948224067688],
                [4.657196894282447, -74.10027533769608]
            ]).addTo(this.mymap);
        } else {
            if (miElemento.value === "Parque el Salitre") {
                var poligono = {
                    "type": "FeatureCollection",
                    "features": [
                      {
                        "type": "Feature",
                        "properties": {
                        },
                        "geometry": {
                          "type": "Polygon",
                          "coordinates": [
                            [
                                [-74.0892219543457,4.671429682682441],
                                [-74.09220457077026,4.6675801214980925],
                                [-74.0924620628357,4.665462853844443],
                                [-74.08969402313232,4.661442166383777],
                                [-74.08913612365723,4.662169313693036],
                                [-74.08802032470703,4.6616774200073525],
                                [-74.0841794013977,4.668606673212742],
                                [-74.0854024887085,4.669569064081119],
                                [-74.08525228500365,4.669975406495796],
                                [-74.08780574798584,4.671878796777595],
                                [-74.08862113952637,4.671001954705308],
                                [-74.0892219543457,4.671429682682441]
                                ]
                            ]}}]}
                var myStyle = {
                    color : "#438f2d"
                };
                L.geoJSON(poligono, {
                    style: function(feature) {
                        feature.properties = myStyle;
                        return feature.properties}
                }).addTo(this.mymap);
                var marker = L.marker(this.salitre).addTo(this.mymap);
                marker.bindPopup("Parque el Salitre").openPopup();
            } else {
                if (miElemento.value === "Parque Tercer Milenio") {
                    var marker = L.marker(this.milenio).addTo(this.mymap);
                    marker.bindPopup("Parque Tercer Milenio").openPopup();
                    var polygon = L.polygon([
                        [4.597263119228281, -74.08431082963943],
                        [4.5970224970267175, -74.08428937196732],
                        [4.596093427208214, -74.08309981226921],
                        [4.5957124413929105, -74.0819478034973],
                        [4.595322097820079, -74.0812075138092],
                        [4.598289773041916, -74.07938361167908],
                        [4.599979470888588, -74.08225893974304],
                        [4.597263119228281, -74.08431082963943]
                    ]).addTo(this.mymap);
                } else {
                    var marker = L.marker(this.panaderia, {icon: this.PanIcono}).addTo(this.mymap);
                    marker.bindPopup("Panaderia cerca a mi casa").openPopup();
                    var polygon = L.polygon([
                        [4.623990391902331, -74.20845553278923],
                        [4.623829983144762, -74.2083790898323],
                        [4.623855381200465, -74.20831605792046],
                        [4.624021136910044, -74.2083965241909],
                        [4.623990391902331, -74.20845553278923]
                    ]).addTo(this.mymap);
                }}}   
    }
}